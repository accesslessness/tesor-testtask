import unittest

from selenium import webdriver
from mytests.page import *


class TestYandexSearch(unittest.TestCase):

    YANDEX_URL = "https://yandex.ru/"
    YANDEX_SEARCH_URL = f"{YANDEX_URL}search"
    YANDEX_IMAGES_URL = f"{YANDEX_URL}images/"
    YANDEX_IMAGES_SEARCH_URL = f"{YANDEX_IMAGES_URL}search"

    def setUp(self):
        self.driver = webdriver.Firefox()

    def tearDown(self) -> None:
        self.driver.quit()

    def test_search(self):
        driver = self.driver
        main_page = YandexMainPage(driver).visit()
        self.assertTrue(main_page.is_open(), "Page wasn't opend")

        search_input = main_page.search_input()
        self.assertTrue(search_input.is_displayed(), "Search input wasn't displayed")
        search_input.send_keys("Тензор")

        suggest_list = main_page.suggest_list()
        main_page.wait().until(lambda _: suggest_list.is_displayed(), "Suggestion list wasn't displayed")

        search_page = main_page.do_search()
        self.assertTrue(search_page.is_open(), "Search results page wasn't opened")

        first_5_results = search_page.results()[:5]
        self.assertTrue(
            any(result.find_elements(By.LINK_TEXT, "tensor.ru") for result in first_5_results),
            "First 5 results must include link to tensor.ru",
        )


    def test_images(self):
        driver = self.driver
        main_page = YandexMainPage(driver).visit()
        self.assertTrue(main_page.is_open(), "Page wasn't opend")

        images_button = main_page.images_button()
        self.assertTrue(images_button.is_displayed(), "Images button isn't there")

        images_button.click()
        driver.switch_to.window(driver.window_handles[-1])
        WebDriverWait(driver, 5).until(
            lambda driver: self.YANDEX_IMAGES_URL in driver.current_url,
            "Yandex Images tab wasn't opened.",
        )

        WebDriverWait(driver, 5).until(
            lambda driver: driver.find_element(By.CSS_SELECTOR, "[id^='PopularRequestList']"),
            "Popular categories weren't displayed.",
        )

        popular_categories = driver.find_element(By.CSS_SELECTOR, "[id^='PopularRequestList']")
        first_category = popular_categories.find_elements(By.CLASS_NAME, "PopularRequestList-Preview")[0]
        category_title = first_category.text

        first_category.click()
        WebDriverWait(driver, 5).until(
            lambda driver: self.YANDEX_IMAGES_SEARCH_URL in driver.current_url,
            "Images search results page wasn't opened",
        )

        search_form = driver.find_element(By.CSS_SELECTOR, "form[action='/images/search'")
        search_input = search_form.find_element(By.CSS_SELECTOR, "input[name='text']")

        displayed_search_text = search_input.get_property("value")
        self.assertEqual(displayed_search_text, category_title, "Search input isn't the same to category name")

        WebDriverWait(driver, 5).until(
            lambda driver: driver.find_elements(By.CLASS_NAME, "serp-item__preview"),
            "Images search results must be open",
        )
        first_preview = driver.find_elements(By.CLASS_NAME, "serp-item__preview")[0]
        first_preview.click()

        WebDriverWait(driver, 5).until(
            lambda driver: "pos=0" in driver.current_url,
            "Image index must be reflected in URL",
        )

        image = driver.find_element(By.CLASS_NAME, "MMImage-Origin")
        first_image_src = image.get_attribute("src")

        next_button = driver.find_element(By.CLASS_NAME, "MediaViewer-ButtonNext")
        next_button.click()

        WebDriverWait(driver, 5).until(
            lambda driver: "pos=1" in driver.current_url,
            "Image index must be reflected in URL",
        )

        image = driver.find_element(By.CLASS_NAME, "MMImage-Origin")
        second_image_src = image.get_attribute("src")
        self.assertNotEqual(first_image_src, second_image_src, "Image didn't change after forth navigation")

        prev_button = driver.find_element(By.CLASS_NAME, "MediaViewer-ButtonPrev")
        prev_button.click()

        WebDriverWait(driver, 10).until(
            lambda drv: "pos=0" in driver.current_url,
            "Image index must be reflected in URL",
        )

        image = driver.find_element(By.CLASS_NAME, "MMImage-Origin")
        back_image_src = image.get_attribute("src")

        self.assertEqual(back_image_src, first_image_src, "Images are not the same after navigation forth & back")



if __name__ == "__main__":
    unittest.main()