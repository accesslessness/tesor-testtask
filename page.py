from typing import List

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.keys import Keys


class BasePage(object):
    def __init__(self, driver):
        self.driver = driver

class YandexSearchPage(BasePage):

    URL = f"https://yandex.ru/search"
    LOC_RESULT_TABLE = (By.ID, "search-result")
    LOC_SEARCH_RESULT_ITEM = (By.CSS_SELECTOR, "li[data-cid]")

    def wait(self):
        return WebDriverWait(self.driver, 5)

    def visit(self):
        self.driver.get(self.URL)
        return self

    def is_open(self) -> bool:
        return self.wait().until(lambda driver: self.URL in driver.current_url)

    def results(self) -> List[WebElement]:
        result_list = self.wait().until(lambda driver: driver.find_element(*self.LOC_RESULT_TABLE))
        return self.wait().until(lambda driver: driver.find_elements(*self.LOC_SEARCH_RESULT_ITEM))

class YandexMainPage(BasePage):

    URL = f"https://yandex.ru/"
    LOC_SEARCH_INPUT = (By.ID, "text")
    LOC_SUGGEST_LIST = (By.CSS_SELECTOR, "ul[id^='suggest-list-']")
    LOC_IMAGES_BUTTON = (By.CSS_SELECTOR, "[data-id='images']")

    def wait(self):
        return WebDriverWait(self.driver, 5)

    def visit(self):
        self.driver.get(self.URL)
        return self

    def is_open(self) -> bool:
        return self.wait().until(lambda driver: self.URL == driver.current_url)

    def search_input(self) -> WebElement:
        return self.wait().until(lambda driver: driver.find_element(*self.LOC_SEARCH_INPUT))

    def suggest_list(self) -> WebElement:
        return self.wait().until(lambda driver: driver.find_element(*self.LOC_SUGGEST_LIST))

    def do_search(self) -> YandexSearchPage:
        self.search_input().send_keys(Keys.RETURN)
        return YandexSearchPage(self.driver)

    def images_button(self) -> WebElement:
        return self.wait().until(lambda driver: driver.find_element(*self.LOC_IMAGES_BUTTON))



